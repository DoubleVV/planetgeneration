#define STB_IMAGE_IMPLEMENTATION

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <stb_image.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <Shader.hpp>
#include <Camera.hpp>

#include <chrono>
#include <random>
#include <iostream>

#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>

#include <Mesh.hpp>

using uint = unsigned int;

typedef std::vector<glm::vec3> VerticesArray;
typedef std::vector<glm::vec3> NormalArray;



namespace Cube{
    enum Face {
        TOP,
        FRONT,
        BACK,
        LEFT,
        BOTTOM,
        RIGHT,
        FACE_COUNT
    };

    enum Edge {
        TOP_EDGE,
        RIGHT_EDGE,
        BOTTOM_EDGE,
        LEFT_EDGE,
        EDGE_COUNT
    };
}


struct Direction{
    int face;
    Cube::Edge edge;

    Direction(int f = Cube::TOP, Cube::Edge e = Cube::TOP_EDGE){
        face = f;
        edge = e;
    }
};

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void processInput(GLFWwindow *window);
unsigned int loadTexture(char const * path);
void generatePlanet(uint size, VerticesArray &verticesArray, NormalArray &normalArray, std::vector<uint> &indices,
                    float smoothing, float radius);
void generateSphereFromCube(uint size, VerticesArray &verticesArray, NormalArray &normalArray, std::vector<uint> &indices,
                            float radius);
std::vector<std::vector<std::vector<double>>> diamantCarreCube(int size, double coef);
double moyenne_carre(int x, int y, int d, std::vector<std::vector<std::vector<double>>>& cube, int face, double coef, float gen);
double moyenne_diamant(int x, int y, int d, int size, std::vector<std::vector<std::vector<double>>>& cube, int face, int coef, float gen);
glm::ivec2 getCoordNextFace(Direction in, Direction out, int x, int y, int size, std::vector<std::vector<std::vector<double>>> &cube);
int normCoord(int x, int size);
double getPointValueFace(std::vector<std::vector<Direction>> topologie, int x, int y, int face, int size, std::vector<std::vector<std::vector<double>>> &cube);
double getValueNextFace(Direction in, Direction out, int x, int y, int size, std::vector<std::vector<std::vector<double>>> &cube);
int norm(int x, int size);
std::vector<std::vector<Direction>> initDirection();
std::vector<std::vector<std::vector<double>>> diamantCarreCube(int size, double coef);
glm::vec3 spherifyPoint(float radius, glm::vec3 &point, double amplitude, uint size, NormalArray &normalArray);


// settings
const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;

// camera
Camera camera(glm::vec3(3.0f, 0.0f, 3.0f));
float lastX = SCR_WIDTH / 2.0f;
float lastY = SCR_HEIGHT / 2.0f;

static bool firstMouse = true;
static bool freeMouse = true;

// timing
float deltaTime = 0.0f;	// time between current frame and last frame
float lastFrame = 0.0f;

// lighting
glm::vec3 lightPos(10.0f, 0.0f, 0.0f);

// topologie
std::vector<std::vector<Direction>> topologie;

int main()
{
    // glfw: initialize and configure
    // ------------------------------
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // uncomment this statement to fix compilation on OS X
#endif

    // glfw window creation
    // --------------------
    GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "LearnOpenGL", NULL, NULL);
    if (window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    glfwSetCursorPosCallback(window, mouse_callback);
    glfwSetScrollCallback(window, scroll_callback);

    // tell GLFW to capture our mouse
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    // glad: load all OpenGL function pointers
    // ---------------------------------------
    glewExperimental = true; // Nécessaire dans le profil de base
    if ( glewInit() != GLEW_OK )
    {
        fprintf( stderr, "Failed to initialize GLEW\n" );
        return -1;
    }

    // configure global opengl state
    // -----------------------------
    glEnable(GL_DEPTH_TEST);

    glEnable( GL_CULL_FACE );

    // Enable blending
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    // IMGUI SETUP
    {
        static const char * glsl_version = "#version 330";

        // Setup Dear ImGui binding
        IMGUI_CHECKVERSION();
        ImGui::CreateContext();
        ImGuiIO & io = ImGui::GetIO();
        ( void )io;
        //io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;  // Enable Keyboard Controls
        //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;   // Enable Gamepad Controls

        ImGui_ImplGlfw_InitForOpenGL( window, true );
        ImGui_ImplOpenGL3_Init( glsl_version );

        // Setup style
        ImGui::StyleColorsDark();
        //ImGui::StyleColorsClassic();
    }

    // build and compile our shader zprogram
    // ------------------------------------
    Shader planetShader("Shader/LightingShaderPlanet.vert", "Shader/LightingShaderPlanet.frag");
    Shader waterShader("Shader/LightingShaderWater.vert", "Shader/LightingShaderWater.frag");
    Shader lampShader("Shader/LampShader.vert", "Shader/LampShader.frag");
    Shader atmosphereShader("Shader/illumination.vert", "Shader/illumination.frag");

    // Variable for HUD

    // Parameter of generation
    uint terrainSize = 257;
    float rotationSpeed = 10.f;
    float smoothing = 1.0f;
    float colorDecal = 0.0f;
    float gradient = 0.18f;
    float radius = 1.f;

    // Read our .obj file
    std::vector<glm::vec3> planetVerticesPosition;
    std::vector<glm::vec3> planetNormals;
    std::vector<uint> planetIndices;
    generatePlanet(terrainSize, planetVerticesPosition, planetNormals, planetIndices, smoothing, radius);
//    generateSphereFromCube( terrainSize, terrainSize, planetVertices, planetNormals, planetIndices );

    std::vector<Vertex> planetVertices;

    for(int i = 0; i < planetVerticesPosition.size(); i++){
        Vertex v;

        v.Position = planetVerticesPosition[i];
        v.Normal = planetNormals[i];
        planetVertices.emplace_back(v);
    }

    Mesh planetMesh(planetVertices, planetIndices, std::vector<Texture>());

    // Water
    std::vector<glm::vec3> waterVerticesPosition;
    std::vector<glm::vec3> waterNormals;
    std::vector<uint> waterIndices;
    generateSphereFromCube(terrainSize, waterVerticesPosition, waterNormals, waterIndices, radius);

    std::vector<Vertex> waterVertices;

    for(int i = 0; i < waterVerticesPosition.size(); i++){
        Vertex v;

        v.Position = waterVerticesPosition[i];
        v.Normal = waterNormals[i];
        waterVertices.emplace_back(v);
    }

    Mesh waterMesh(waterVertices, waterIndices, std::vector<Texture>());

    // LightMesh
    std::vector<Vertex> lightVertices;

    for(int i = 0; i < waterVerticesPosition.size(); i++){
        Vertex v;

        v.Position = waterVerticesPosition[i];
        v.Normal = waterNormals[i];
        lightVertices.emplace_back(v);
    }

    Mesh lightMesh(lightVertices, waterIndices, std::vector<Texture>());

    const auto send_terrain([&](){

        planetMesh.clear();
        planetVertices.clear();

        waterMesh.clear();
        waterVertices.clear();

        for(int i = 0; i < planetVerticesPosition.size(); i++){
            Vertex v;

            v.Position = planetVerticesPosition[i];
            v.Normal = planetNormals[i];
            planetVertices.emplace_back(v);
        }

        planetMesh.setVertices(planetVertices);
        planetMesh.setIndices(planetIndices);
        planetMesh.setTextures(std::vector<Texture>());

        planetMesh.setupMesh();

        for(int i = 0; i < waterVerticesPosition.size(); i++){
            Vertex v;

            v.Position = waterVerticesPosition[i];
            v.Normal = waterNormals[i];
            waterVertices.emplace_back(v);
        }

        waterMesh.setVertices(waterVertices);
        waterMesh.setIndices(waterIndices);
        waterMesh.setTextures(std::vector<Texture>());

        waterMesh.setupMesh();
    });

    // load textures (we now use a utility function to keep the code more organized)
    // -----------------------------------------------------------------------------
//    unsigned int diffuseMap = loadTexture("Media/container2.png");
//    unsigned int specularMap = loadTexture("Media/container2_specular.png");

    // render loop
    // -----------
    while (!glfwWindowShouldClose(window))
    {
        // per-frame time logic
        // --------------------
        float currentFrame = glfwGetTime();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;

        // input
        // -----
        processInput(window);

        // render
        // ------
        glClearColor(0.05f, 0.05f, 0.05f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        if ( !freeMouse ) glfwSetCursorPos( window, SCR_WIDTH / 2, SCR_HEIGHT / 2 ); // prevent mouse force

        // IMGUI HERE
        {
            ImGui_ImplOpenGL3_NewFrame();
            ImGui_ImplGlfw_NewFrame();
            ImGui::NewFrame();

            // 2. Show a simple window that we create ourselves. We use a Begin/End pair to created a named window.
            {
                ImGui::Begin( "Hello, world!" );                        // Create a window called "Hello, world!" and append into it.

                ImGui::SliderFloat( "DecalColor", &colorDecal, -1.f, 1.f );
                ImGui::SliderFloat( "Gradient", &gradient, 0.f, 1.f );
                ImGui::SliderFloat( "Rayon", &radius, 1.f, 2.f );
                ImGui::SliderFloat( "Lissage", &smoothing, 0.f, 3.f );
                ImGui::SliderFloat( "Vitesse de rotation", &rotationSpeed, 0.f, 20.f );

                if ( ImGui::Button( "RegenTerrain" ) )
                {
                    generatePlanet(terrainSize, planetVerticesPosition, planetNormals, planetIndices, smoothing, radius);
                    generateSphereFromCube(terrainSize, waterVerticesPosition, waterNormals, waterIndices, radius);
                    send_terrain();
                }

//                ImGui::Checkbox( "GL_POINTS ?", &gl_points_mode );

                ImGui::Text( "Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate );
                ImGui::End();
            }

            ImGui::Render();
        }

        planetShader.use();
        glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);
        glm::mat4 view = camera.GetViewMatrix();

        planetShader.setMat4("projection", projection);
        planetShader.setMat4("view", view);

        // world transformation
        glm::mat4 model(1.0f);

        auto rotateModel = glm::rotate(model, (float)glm::radians(currentFrame*rotationSpeed), glm::vec3(0,1,0));
        planetShader.setMat4("model", rotateModel);
//        planetShader.setMat4("model", model);

        planetShader.setVec3("light.position", lightPos);
        planetShader.setVec3("light.direction", -1.0f, 0.0f, 0.0f);
        planetShader.setVec3("viewPos", camera.Position);

        planetShader.setVec3("light.ambient",  1.0f, 1.0f, 1.0f);
        planetShader.setVec3("light.diffuse",  1.0f, 1.0f, 1.0f); // darken the light a bit to fit the scene
        planetShader.setVec3("light.specular", 1.0f, 1.0f, 1.0f);

        // bind diffuse map
//        glActiveTexture(GL_TEXTURE0);
//        glBindTexture(GL_TEXTURE_2D, diffuseMap);
//
//        glActiveTexture(GL_TEXTURE1);
//        glBindTexture(GL_TEXTURE_2D, specularMap);

        planetShader.setVec3("material.ambient",  0.0f, 0.0f, 0.0f);
        planetShader.setVec3("material.diffuse", 0.8f, 0.8f, 0.8f);
        planetShader.setVec3("material.specular", 0.0f, 0.0f, 0.0f);
        planetShader.setFloat("material.shininess", 2.0f);

        planetShader.setFloat("gradient", gradient);
        planetShader.setFloat("radius", radius);
        planetShader.setFloat("colorDecal", colorDecal);

        // render the cube
        planetMesh.Draw(planetShader);

        // be sure to activate shader when setting uniforms/drawing objects
        waterShader.use();
        waterShader.setVec3("light.position", lightPos);
        waterShader.setVec3("viewPos", camera.Position);

        waterShader.setVec3("light.ambient",  1.0f, 1.0f, 1.0f);
        waterShader.setVec3("light.diffuse",  1.0f, 1.0f, 1.0f); // darken the light a bit to fit the scene
        waterShader.setVec3("light.specular", 1.0f, 1.0f, 1.0f);

        waterShader.setVec3("material.ambient",  0.0f, 0.0f, 0.0f);
        waterShader.setVec3("material.diffuse", 0.0f, 0.5f, 1.0f);
        waterShader.setVec3("material.specular", 0.8f, 0.8f, 0.8f);
        waterShader.setFloat("material.shininess", 4.0f);

        glm::vec3 lightColor(1.f, 1.f, 1.f);
//        lightColor.x = sin(glfwGetTime() * 2.0f);
//        lightColor.y = sin(glfwGetTime() * 0.7f);
//        lightColor.z = sin(glfwGetTime() * 1.3f);
//
//        glm::vec3 diffuseColor = lightColor   * glm::vec3(0.5f); // decrease the influence
//        glm::vec3 ambientColor = diffuseColor * glm::vec3(0.2f); // low influence
//
//        lightingShader.setVec3("light.ambient", ambientColor);
//        lightingShader.setVec3("light.diffuse", diffuseColor);

        // view/projection transformations

        waterShader.setMat4("projection", projection);
        waterShader.setMat4("view", view);

        auto scaleModel = glm::scale(model, glm::vec3(1.f - colorDecal));
        waterShader.setMat4("model", scaleModel);

//        glEnable(GL_BLEND);
//        glBlendFunc(GL_ONE, GL_ONE);

//        atmosphereShader.use();
//
//        glm::mat4 tv = glm::transpose(model * view);
//        glm::vec4 camMult = {-tv[3][0], -tv[3][1], -tv[3][2], -tv[3][3]};
//        glm::vec4 camPosition = tv * camMult;
//
//        atmosphereShader.setMat4("projection", projection);
//        atmosphereShader.setMat4("view", view);
//        atmosphereShader.setMat4("model", model);
//        atmosphereShader.setVec3("camPosition", camera.Position);
//        atmosphereShader.setFloat("fInnerRadius", 0.7f);
//        atmosphereShader.setFloat("fOuterRadius", 1.2f);


        waterMesh.Draw(waterShader);

//        glDisable(GL_BLEND);
//        glFrontFace(GL_CCW);

//        glBindVertexArray(waterVAO);
//        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
//        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, waterEBO);
//        glDrawElements(GL_TRIANGLES, waterIndices.size(), GL_UNSIGNED_INT, nullptr);
//        glBindVertexArray(0);

        // also draw the lamp object
        lampShader.use();
        lampShader.setMat4("projection", projection);
        lampShader.setMat4("view", view);
        model = glm::mat4(1.0f);
        model = glm::translate(model, lightPos);
        model = glm::scale(model, glm::vec3(0.2f)); // a smaller cube
        lampShader.setMat4("model", model);
        lampShader.setVec3("lightColor",  lightColor);

        lightMesh.Draw(lampShader);

        // Draw Imgui
        ImGui_ImplOpenGL3_RenderDrawData( ImGui::GetDrawData() );

        // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
        // -------------------------------------------------------------------------------
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    // optional: de-allocate all resources once they've outlived their purpose:
    // ------------------------------------------------------------------------

    // Cleanup IMGUI
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    // glfw: terminate, clearing all previously allocated GLFW resources.
    // ------------------------------------------------------------------
    glfwTerminate();
    return 0;
}

// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void processInput(GLFWwindow *window)
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);

    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
        camera.ProcessKeyboard(FORWARD, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
        camera.ProcessKeyboard(BACKWARD, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
        camera.ProcessKeyboard(LEFT, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
        camera.ProcessKeyboard(RIGHT, deltaTime);
    if ( glfwGetKey( window, GLFW_KEY_P ) == GLFW_PRESS )
        glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
    if ( glfwGetKey( window, GLFW_KEY_O ) == GLFW_PRESS )
        glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
    if ( glfwGetKey( window, GLFW_KEY_LEFT_SHIFT ) == GLFW_PRESS || glfwGetKey( window, GLFW_KEY_RIGHT_SHIFT ) == GLFW_PRESS )
    {
        glfwSetInputMode( window, GLFW_CURSOR, GLFW_CURSOR_DISABLED );
        firstMouse = true;
        freeMouse = false;
    }
    else
    {
        glfwSetInputMode( window, GLFW_CURSOR, GLFW_CURSOR_NORMAL );
        freeMouse = true;
    }
}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    // make sure the viewport matches the new window dimensions; note that width and
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);
}


// glfw: whenever the mouse moves, this callback is called
// -------------------------------------------------------
void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
    if(freeMouse){
        return;
    }
    if (firstMouse)
    {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }

    float xoffset = xpos - lastX;
    float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top

    lastX = xpos;
    lastY = ypos;

    camera.ProcessMouseMovement(xoffset, yoffset);
}

// glfw: whenever the mouse scroll wheel scrolls, this callback is called
// ----------------------------------------------------------------------
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
    camera.ProcessMouseScroll(yoffset);
}

// utility function for loading a 2D texture from file
// ---------------------------------------------------
unsigned int loadTexture(char const * path)
{
    unsigned int textureID;
    glGenTextures(1, &textureID);

    int width, height, nrComponents;
    unsigned char *data = stbi_load(path, &width, &height, &nrComponents, 0);
    if (data)
    {
        GLenum format;
        if (nrComponents == 1)
            format = GL_RED;
        else if (nrComponents == 3)
            format = GL_RGB;
        else if (nrComponents == 4)
            format = GL_RGBA;

        glBindTexture(GL_TEXTURE_2D, textureID);
        glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        stbi_image_free(data);
    }
    else
    {
        std::cout << "Texture failed to load at path: " << path << std::endl;
        stbi_image_free(data);
    }

    return textureID;
}

void generateSphereFromCube(uint size, VerticesArray &verticesArray, NormalArray &normalArray, std::vector<uint> &indices,
                            float radius) {
    if ( size < 2 )
        throw std::runtime_error( std::string( __func__ ) + "Need size > 3" );

    //
    verticesArray.clear();
    normalArray.clear();
    indices.clear();

    std::vector<std::vector<std::vector<double>>> cube;

    for(int i = 0; i < Cube::FACE_COUNT; i++){
        cube.emplace_back(std::vector<std::vector<double>>(size, std::vector<double>(size, 0)));
    }

    const uint square( size * size );
    const uint maxFace(Cube::FACE_COUNT);

    glm::vec3 center(0.f, 0.f, 0.f);

    float halfSize = float(size)/2;
    double step = double(size)/(size-1);

    for(int currentFace = 0; currentFace < maxFace; currentFace++){
        for ( uint i( 0 ); i < square; ++i ) {
            switch (currentFace) {
                case Cube::TOP :
                    verticesArray.emplace_back(
                            glm::vec3(((i % (size)) * step) - halfSize,
                                      (0) + (center.y + halfSize),
                                      (int(i / (size)) * step) - halfSize));
                    normalArray.emplace_back(spherifyPoint(radius, verticesArray[verticesArray.size() - 1], cube[currentFace][i % size][i / size], size, normalArray));
                    break;
                case Cube::BOTTOM :
                    verticesArray.emplace_back(
                            glm::vec3(((i % (size)) * step) - halfSize,
                                      -(0) + (center.y - halfSize),
                                      -((i / (size)) * step) + halfSize));
                    normalArray.emplace_back(spherifyPoint(radius, verticesArray[verticesArray.size() - 1], cube[currentFace][i % size][i / size], size, normalArray));
                    break;
                case Cube::LEFT :
                    verticesArray.emplace_back(
                            glm::vec3(-(0) + (center.x - halfSize),
                                      ((i % size) * step) - halfSize, ((i / size) * step) - halfSize));
                    normalArray.emplace_back(spherifyPoint(radius, verticesArray[verticesArray.size() - 1], cube[currentFace][i % size][i / size], size, normalArray));
                    break;
                case Cube::RIGHT :
                    verticesArray.emplace_back(
                            glm::vec3((0) + (center.x + halfSize),
                                      (halfSize - (i % size) * step), ((i / size) * step) - halfSize));
                    normalArray.emplace_back(spherifyPoint(radius, verticesArray[verticesArray.size() - 1], cube[currentFace][i % size][i / size], size, normalArray));
                    break;
                case Cube::FRONT :
                    verticesArray.emplace_back(
                            glm::vec3(((i % size) * step) - halfSize, halfSize - ((i / size) * step),
                                      (0) + (center.z + halfSize)));
                    normalArray.emplace_back(spherifyPoint(radius, verticesArray[verticesArray.size() - 1], cube[currentFace][i % size][i / size], size, normalArray));
                    break;
                case Cube::BACK :
                    verticesArray.emplace_back(
                            glm::vec3(((i % size) * step) - halfSize, ((i / size) * step) - halfSize,
                                      -(0) + (center.z - halfSize)));
                    normalArray.emplace_back(spherifyPoint(radius, verticesArray[verticesArray.size() - 1], cube[currentFace][i % size][i / size], size, normalArray));
                    break;
                default :
                    break;
            }
        }
    }

    for(int currentFace = 0; currentFace < maxFace; currentFace++){
        for(int j = 0; j < (size - 1); j++){
            for(int i = 0; i < size - 1; i++){
                indices.emplace_back(i + (j*(size)) + (currentFace * (size * (size))));
                indices.emplace_back(i + (j+1)*(size) + (currentFace * (size * (size))));
                indices.emplace_back(i+1 + (j+1)*(size) + (currentFace * (size * (size))));

                indices.emplace_back(i + j*(size) + (currentFace * (size * (size))));
                indices.emplace_back(i+1 + (j+1)*(size) + (currentFace * (size * (size))));
                indices.emplace_back(i+1 + (j*(size)) + (currentFace * (size * (size))));
            }
        }
    }
}

void calculeNormal(VerticesArray &verticesArray, NormalArray &normalArray, int face, std::vector<std::vector<std::vector<double>>>& cube, int size, int i){
    glm::vec3 normal(0.f);


    glm::vec3 current;
    if(i%size == 0){
        auto adjacentDirection = topologie[face][Cube::LEFT_EDGE];
        auto adjacentCood = getCoordNextFace(adjacentDirection, Direction(face, Cube::LEFT_EDGE), (i % size) - 1, i/ size, size, cube);

        current = verticesArray[(adjacentDirection.face*size*size) + adjacentCood.x * size + adjacentCood.y];
    }
    else {
        current = verticesArray[i - 1];

    }
    glm::vec3 next = verticesArray[i];

    normal.x += (current.y - next.y) * (current.z + next.z);
    normal.y += (current.z - next.z) * (current.x + next.x);
    normal.z += (current.x - next.x) * (current.y + next.y);

    auto length = sqrt((normal.x * normal.x) + (normal.y * normal.y) + (normal.z * normal.z));
    normal.x = normal.x/length;
    normal.y = normal.y/length;
    normal.z = normal.z/length;

    normalArray.emplace_back(normal);
}

void generatePlanet(uint size, VerticesArray &verticesArray, NormalArray &normalArray, std::vector<uint> &indices,
                    float smoothing, float radius) {
    if ( size < 2 )
        throw std::runtime_error( std::string( __func__ ) + "Need size > 3" );

    //
    verticesArray.clear();
    normalArray.clear();
    indices.clear();

    std::vector<glm::vec3> faceNormal;
    std::vector<std::vector<std::vector<glm::vec3>>> cube;

    for(int i = 0; i < Cube::FACE_COUNT; i++){
        cube.emplace_back(std::vector<std::vector<glm::vec3>>(size, std::vector<glm::vec3>(size, glm::vec3(0.f))));
    }

    auto heightMapCube = diamantCarreCube(size, smoothing);
    const uint square( size * size );
    const uint maxFace(Cube::FACE_COUNT);

    glm::vec3 center(0.f, 0.f, 0.f);

    float halfSize = float(size)/2;
    double step = double(size)/(size-1);

    for(int currentFace = 0; currentFace < maxFace; currentFace++){
        for ( uint i( 0 ); i < square; ++i ) {
            switch (currentFace) {
                case Cube::TOP :
                    verticesArray.emplace_back(
                            glm::vec3(((i % (size)) * step) - halfSize,
                                      (0) + (center.y + halfSize),
                                      (int(i / (size)) * step) - halfSize));
                    cube[currentFace][i%size][i/size] = spherifyPoint(radius, verticesArray[verticesArray.size() - 1], heightMapCube[currentFace][i % size][i / size], size, normalArray);
//                    calculeNormal(verticesArray, normalArray, currentFace, heightMapCube, size, i);
                    break;
                case Cube::BOTTOM :
                    verticesArray.emplace_back(
                            glm::vec3(((i % (size)) * step) - halfSize,
                                      -(0) + (center.y - halfSize),
                                      -((i / (size)) * step) + halfSize));
                    cube[currentFace][i%size][i/size] = spherifyPoint(radius, verticesArray[verticesArray.size() - 1], heightMapCube[currentFace][i % size][i / size], size, normalArray);
//                    calculeNormal(verticesArray, normalArray, currentFace, heightMapCube, size, i);
                    break;
                case Cube::LEFT :
                    verticesArray.emplace_back(
                            glm::vec3(-(0) + (center.x - halfSize),
                                      ((i % size) * step) - halfSize, ((i / size) * step) - halfSize));
                    cube[currentFace][i%size][i/size] = spherifyPoint(radius, verticesArray[verticesArray.size() - 1], heightMapCube[currentFace][i % size][i / size], size, normalArray);
//                    calculeNormal(verticesArray, normalArray, currentFace, heightMapCube, size, i);
                    break;
                case Cube::RIGHT :
                    verticesArray.emplace_back(
                            glm::vec3((0) + (center.x + halfSize),
                                      (halfSize - (i % size) * step), ((i / size) * step) - halfSize));
                    cube[currentFace][i%size][i/size] = spherifyPoint(radius, verticesArray[verticesArray.size() - 1], heightMapCube[currentFace][i % size][i / size], size, normalArray);
//                    calculeNormal(verticesArray, normalArray, currentFace, heightMapCube, size, i);
                    break;
                case Cube::FRONT :
                    verticesArray.emplace_back(
                            glm::vec3(((i % size) * step) - halfSize, halfSize - ((i / size) * step),
                                      (0) + (center.z + halfSize)));
                    cube[currentFace][i%size][i/size] = spherifyPoint(radius, verticesArray[verticesArray.size() - 1], heightMapCube[currentFace][i % size][i / size], size, normalArray);
//                    calculeNormal(verticesArray, normalArray, currentFace, heightMapCube, size, i);
                    break;
                case Cube::BACK :
                    verticesArray.emplace_back(
                            glm::vec3(((i % size) * step) - halfSize, ((i / size) * step) - halfSize,
                                      -(0) + (center.z - halfSize)));
                    cube[currentFace][i%size][i/size] = spherifyPoint(radius, verticesArray[verticesArray.size() - 1], heightMapCube[currentFace][i % size][i / size], size, normalArray);
//                    calculeNormal(verticesArray, normalArray, currentFace, heightMapCube, size, i);
                    break;
                default :
                    break;
            }
        }
    }

    for(int currentFace = 0; currentFace < maxFace; currentFace++){
        for(int j = 0; j < (size - 1); j++){
            for(int i = 0; i < size - 1; i++){
                glm::vec3 v1;
                v1.x = verticesArray[i + (j*(size)) + (currentFace * (size * (size)))].x - verticesArray[i+1 + (j+1)*(size) + (currentFace * (size * (size)))].x;
                v1.y = verticesArray[i + (j*(size)) + (currentFace * (size * (size)))].y - verticesArray[i+1 + (j+1)*(size) + (currentFace * (size * (size)))].y;
                v1.z = verticesArray[i + (j*(size)) + (currentFace * (size * (size)))].z - verticesArray[i+1 + (j+1)*(size) + (currentFace * (size * (size)))].z;
                glm::vec3 v2;
                v2.x = verticesArray[i + (j+1)*(size) + (currentFace * (size * (size)))].x - verticesArray[i+1 + (j+1)*(size) + (currentFace * (size * (size)))].x;
                v2.y = verticesArray[i + (j+1)*(size) + (currentFace * (size * (size)))].y - verticesArray[i+1 + (j+1)*(size) + (currentFace * (size * (size)))].y;
                v2.z = verticesArray[i + (j+1)*(size) + (currentFace * (size * (size)))].z - verticesArray[i+1 + (j+1)*(size) + (currentFace * (size * (size)))].z;

                auto vCross = glm::cross(v1, v2);

                auto length = sqrt((vCross.x * vCross.x) + (vCross.y * vCross.y) + (vCross.z * vCross.z));
                vCross.x = vCross.x / length;
                vCross.y = vCross.y / length;
                vCross.z = vCross.z / length;

                faceNormal.emplace_back(vCross);

                indices.emplace_back(i + (j*(size)) + (currentFace * (size * (size))));
                indices.emplace_back(i + (j+1)*(size) + (currentFace * (size * (size))));
                indices.emplace_back(i+1 + (j+1)*(size) + (currentFace * (size * (size))));

                v1.x = verticesArray[i + j*(size) + (currentFace * (size * (size)))].x - verticesArray[i+1 + (j*(size)) + (currentFace * (size * (size)))].x;
                v1.y = verticesArray[i + j*(size) + (currentFace * (size * (size)))].y - verticesArray[i+1 + (j*(size)) + (currentFace * (size * (size)))].y;
                v1.z = verticesArray[i + j*(size) + (currentFace * (size * (size)))].z - verticesArray[i+1 + (j*(size)) + (currentFace * (size * (size)))].z;
                v2.x = verticesArray[i+1 + (j+1)*(size) + (currentFace * (size * (size)))].x - verticesArray[i+1 + (j*(size)) + (currentFace * (size * (size)))].x;
                v2.y = verticesArray[i+1 + (j+1)*(size) + (currentFace * (size * (size)))].y - verticesArray[i+1 + (j*(size)) + (currentFace * (size * (size)))].y;
                v2.z = verticesArray[i+1 + (j+1)*(size) + (currentFace * (size * (size)))].z - verticesArray[i+1 + (j*(size)) + (currentFace * (size * (size)))].z;

                vCross = glm::cross(v1, v2);

                length = sqrt((vCross.x * vCross.x) + (vCross.y * vCross.y) + (vCross.z * vCross.z));
                vCross.x = vCross.x / length;
                vCross.y = vCross.y / length;
                vCross.z = vCross.z / length;

                faceNormal.emplace_back(vCross);

                indices.emplace_back(i + j*(size) + (currentFace * (size * (size))));
                indices.emplace_back(i+1 + (j+1)*(size) + (currentFace * (size * (size))));
                indices.emplace_back(i+1 + (j*(size)) + (currentFace * (size * (size))));
            }
        }
    }

    // Calcule des normales
    for(int currentFace = 0; currentFace < maxFace; currentFace++) {
        for (int y = 0; y < size; y++) {
            for (int x = 0; x < size; x++) {
//                glm::vec3 normal(0.f);
//                glm::vec3 next(0.f);
//
//                glm::vec3 current = cube[currentFace][x][y];
//                if(x == size - 1)
//                    next = cube[currentFace][x][y];
//                else
//                    next = cube[currentFace][x+1][y];
//
//                normal.x += (current.y - next.y) * (current.z + next.z);
//                normal.y += (current.z - next.z) * (current.x + next.x);
//                normal.z += (current.x - next.x) * (current.y + next.y);
//
//                auto length = sqrt((normal.x * normal.x) + (normal.y * normal.y) + (normal.z * normal.z));
//                normal.x = normal.x / length;
//                normal.y = normal.y / length;
//                normal.z = normal.z / length;
//
//                normalArray.emplace_back(normal);
                normalArray.emplace_back(cube[currentFace][x][y]);
            }
        }
    }
}

std::vector<std::vector<std::vector<double>>> diamantCarreCube(int size, double coef){
    std::vector<std::vector<std::vector<double>>> cube;

    for(int i = 0; i < Cube::FACE_COUNT; i++){
        cube.emplace_back(std::vector<std::vector<double>>(size, std::vector<double>(size, 0)));
    }

    std::cout << "Début du diamant carré" << std::endl;


    auto seed1 = std::chrono::system_clock::now().time_since_epoch().count();
    std::mt19937 gen(seed1);
    std::uniform_real_distribution<float> dis(-size, size);

    std::cout << "génération ... " << std::endl;

    topologie = initDirection();

    int s = size - 1;

    cube[Cube::TOP][0][0] = dis(gen);
    cube[Cube::TOP][s][0] = dis(gen); // Cube::BACK, Cube::LEFT
    cube[Cube::TOP][0][s] = dis(gen); // Cube::LEFT, Cube::FRONT
    cube[Cube::TOP][s][s] = dis(gen); // Cube::RIGHT, Cube::FRONT

    cube[Cube::BOTTOM][0][0] = dis(gen); // Cube::BACK, Cube::RIGHT
    cube[Cube::BOTTOM][s][0] = dis(gen); // Cube::BACK, Cube::LEFT
    cube[Cube::BOTTOM][0][s] = dis(gen); // Cube::LEFT, Cube::FRONT
    cube[Cube::BOTTOM][s][s] = dis(gen); // Cube::RIGHT, Cube::FRONT

    cube[Cube::LEFT][0][0] = cube[Cube::BOTTOM][0][s]; // Cube::BACK, Cube::RIGHT
    cube[Cube::LEFT][s][0] = cube[Cube::TOP][0][0]; // Cube::BACK, Cube::LEFT
    cube[Cube::LEFT][0][s] = cube[Cube::BOTTOM][0][0]; // Cube::LEFT, Cube::FRONT
    cube[Cube::LEFT][s][s] = cube[Cube::TOP][0][s]; // Cube::RIGHT, Cube::FRONT

    cube[Cube::RIGHT][0][0] = cube[Cube::TOP][s][0]; // Cube::BACK, Cube::RIGHT
    cube[Cube::RIGHT][s][0] = cube[Cube::BOTTOM][s][s]; // Cube::BACK, Cube::LEFT
    cube[Cube::RIGHT][0][s] = cube[Cube::TOP][s][s]; // Cube::LEFT, Cube::FRONT
    cube[Cube::RIGHT][s][s] = cube[Cube::BOTTOM][s][0]; // Cube::RIGHT, Cube::FRONT

    cube[Cube::FRONT][0][0] = cube[Cube::TOP][0][s]; // Cube::BACK, Cube::RIGHT
    cube[Cube::FRONT][s][0] = cube[Cube::TOP][s][s]; // Cube::BACK, Cube::LEFT
    cube[Cube::FRONT][0][s] = cube[Cube::BOTTOM][0][0]; // Cube::LEFT, Cube::FRONT
    cube[Cube::FRONT][s][s] = cube[Cube::BOTTOM][s][0]; // Cube::RIGHT, Cube::FRONT

    cube[Cube::BACK][0][0] = cube[Cube::BOTTOM][0][s]; // Cube::BACK, Cube::RIGHT
    cube[Cube::BACK][s][0] = cube[Cube::BOTTOM][s][s]; // Cube::BACK, Cube::LEFT
    cube[Cube::BACK][0][s] = cube[Cube::TOP][0][0]; // Cube::LEFT, Cube::FRONT
    cube[Cube::BACK][s][s] = cube[Cube::TOP][s][0]; // Cube::RIGHT, Cube::FRONT

    for(int d = size-1; d > 1; d>>=1) {
        std::uniform_real_distribution<float> dis2(-d, d);
        for(int face = 0; face < Cube::FACE_COUNT; face++) {

            /* CARRE */
            for (int x = d / 2; x < size; x += d)
                for (int y = d / 2; y < size; y += d) {
                    cube[face][x][y] = moyenne_carre(x, y, d, cube, face, coef, dis2(gen));
                }
        }
        for(int face = 0; face < Cube::FACE_COUNT; face++) {
            /* DIAMANT */
            for (int x = 0; x < size; x += d)
                for (int y = d / 2; y < size; y += d) {
                    if(cube[face][x][y] == 0){
                        cube[face][x][y] = moyenne_diamant(x, y, d, size, cube, face, coef, dis2(gen));
                        if(x == 0){
                            auto adjacentDirection = topologie[face][Cube::LEFT_EDGE];
                            auto adjacentCood = getCoordNextFace(adjacentDirection, Direction(face, Cube::LEFT_EDGE), x, y, size, cube);

                            cube[adjacentDirection.face][adjacentCood.x][adjacentCood.y] = cube[face][x][y];
                        }
                        if(x == size - 1){
                            auto adjacentDirection = topologie[face][Cube::RIGHT_EDGE];
                            auto adjacentCood = getCoordNextFace(adjacentDirection, Direction(face, Cube::RIGHT_EDGE), x, y, size, cube);

                            cube[adjacentDirection.face][adjacentCood.x][adjacentCood.y] = cube[face][x][y];
                        }
                        if(y == 0){
                            auto adjacentDirection = topologie[face][Cube::TOP_EDGE];
                            auto adjacentCood = getCoordNextFace(adjacentDirection, Direction(face, Cube::TOP_EDGE), x, y, size, cube);

                            cube[adjacentDirection.face][adjacentCood.x][adjacentCood.y] = cube[face][x][y];
                        }
                        if(y == size - 1){
                            auto adjacentDirection = topologie[face][Cube::BOTTOM_EDGE];
                            auto adjacentCood = getCoordNextFace(adjacentDirection, Direction(face, Cube::BOTTOM_EDGE), x, y, size, cube);

                            cube[adjacentDirection.face][adjacentCood.x][adjacentCood.y] = cube[face][x][y];
                        }
                    }
                    if(cube[face][y][x] == 0){
                        cube[face][y][x] = moyenne_diamant(y, x, d, size, cube, face, coef, dis2(gen));
                        if(y == 0){
                            auto adjacentDirection = topologie[face][Cube::LEFT_EDGE];
                            auto adjacentCood = getCoordNextFace(adjacentDirection, Direction(face, Cube::LEFT_EDGE), x, y, size, cube);

                            cube[adjacentDirection.face][adjacentCood.y][adjacentCood.x] = cube[face][y][x];
                        }
                        if(y == size - 1){
                            auto adjacentDirection = topologie[face][Cube::RIGHT_EDGE];
                            auto adjacentCood = getCoordNextFace(adjacentDirection, Direction(face, Cube::RIGHT_EDGE), x, y, size, cube);

                            cube[adjacentDirection.face][adjacentCood.y][adjacentCood.x] = cube[face][y][x];
                        }
                        if(x == 0){
                            auto adjacentDirection = topologie[face][Cube::TOP_EDGE];
                            auto adjacentCood = getCoordNextFace(adjacentDirection, Direction(face, Cube::TOP_EDGE), x, y, size, cube);

                            cube[adjacentDirection.face][adjacentCood.y][adjacentCood.x] = cube[face][y][x];
                        }
                        if(x == size - 1){
                            auto adjacentDirection = topologie[face][Cube::BOTTOM_EDGE];
                            auto adjacentCood = getCoordNextFace(adjacentDirection, Direction(face, Cube::BOTTOM_EDGE), x, y, size, cube);

                            cube[adjacentDirection.face][adjacentCood.y][adjacentCood.x] = cube[face][y][x];
                        }
                    }
                }
        }

        std::cout << "done" << std::endl;
    }
    return cube;
}


std::vector<std::vector<Direction>> initDirection(){
    std::vector<std::vector<Direction>> direction(Cube::FACE_COUNT, std::vector<Direction>(Cube::EDGE_COUNT));

    direction[Cube::TOP][Cube::TOP_EDGE] = Direction(Cube::BACK, Cube::BOTTOM_EDGE);
    direction[Cube::TOP][Cube::LEFT_EDGE] = Direction(Cube::LEFT, Cube::RIGHT_EDGE);
    direction[Cube::TOP][Cube::BOTTOM_EDGE] = Direction(Cube::FRONT, Cube::TOP_EDGE);
    direction[Cube::TOP][Cube::RIGHT_EDGE] = Direction(Cube::RIGHT, Cube::LEFT_EDGE);

    direction[Cube::FRONT][Cube::TOP_EDGE] = Direction(Cube::TOP, Cube::BOTTOM_EDGE);
    direction[Cube::FRONT][Cube::LEFT_EDGE] = Direction(Cube::LEFT, Cube::BOTTOM_EDGE);
    direction[Cube::FRONT][Cube::BOTTOM_EDGE] = Direction(Cube::BOTTOM, Cube::TOP_EDGE);
    direction[Cube::FRONT][Cube::RIGHT_EDGE] = Direction(Cube::RIGHT, Cube::BOTTOM_EDGE);

    direction[Cube::BOTTOM][Cube::TOP_EDGE] = Direction(Cube::FRONT, Cube::BOTTOM_EDGE);
    direction[Cube::BOTTOM][Cube::LEFT_EDGE] = Direction(Cube::LEFT, Cube::LEFT_EDGE);
    direction[Cube::BOTTOM][Cube::BOTTOM_EDGE] = Direction(Cube::BACK, Cube::TOP_EDGE);
    direction[Cube::BOTTOM][Cube::RIGHT_EDGE] = Direction(Cube::RIGHT, Cube::RIGHT_EDGE);

    direction[Cube::BACK][Cube::TOP_EDGE] = Direction(Cube::BOTTOM, Cube::BOTTOM_EDGE);
    direction[Cube::BACK][Cube::LEFT_EDGE] = Direction(Cube::LEFT, Cube::TOP_EDGE);
    direction[Cube::BACK][Cube::BOTTOM_EDGE] = Direction(Cube::TOP, Cube::TOP_EDGE);
    direction[Cube::BACK][Cube::RIGHT_EDGE] = Direction(Cube::RIGHT, Cube::TOP_EDGE);

    direction[Cube::LEFT][Cube::TOP_EDGE] = Direction(Cube::BACK, Cube::LEFT_EDGE);
    direction[Cube::LEFT][Cube::LEFT_EDGE] = Direction(Cube::BOTTOM, Cube::LEFT_EDGE);
    direction[Cube::LEFT][Cube::BOTTOM_EDGE] = Direction(Cube::FRONT, Cube::LEFT_EDGE);
    direction[Cube::LEFT][Cube::RIGHT_EDGE] = Direction(Cube::TOP, Cube::LEFT_EDGE);

    direction[Cube::RIGHT][Cube::TOP_EDGE] = Direction(Cube::BACK, Cube::RIGHT_EDGE);
    direction[Cube::RIGHT][Cube::LEFT_EDGE] = Direction(Cube::TOP, Cube::RIGHT_EDGE);
    direction[Cube::RIGHT][Cube::BOTTOM_EDGE] = Direction(Cube::FRONT, Cube::RIGHT_EDGE);
    direction[Cube::RIGHT][Cube::RIGHT_EDGE] = Direction(Cube::BOTTOM, Cube::RIGHT_EDGE);

    return direction;
}

int norm(int x, int size){
    if(x >= size){
        x = x - size + 1;
    }
    if(x < 0){
        x = x + size - 1;
    }
    if(x < 0){
        x = x + size - 1;
    }

    return x;
}

double getValueNextFace(Direction in, Direction out, int x, int y, int size,
                        std::vector<std::vector<std::vector<double>>> &cube){
    std::vector<glm::vec3> movementVector{glm::vec3(-1,0,0), glm::vec3(0,1,0), glm::vec3(1,0,0), glm::vec3(0,-1,0)};

    auto crossProduct = glm::cross(movementVector[in.edge], movementVector[out.edge]).z;

    if(crossProduct == 0){
        if(in.edge == out.edge){
            return cube[in.face][norm(-x, size)][norm(-y, size)];
        }
        else{
            if(movementVector[in.edge].x == 0){
                return cube[in.face][norm(x, size)][norm(y,size)];
            }
            else{
                return cube[in.face][norm(x, size)][norm(y, size)];
            }
        }
    }
    if(crossProduct < 0){
        return cube[in.face][norm((-y), size)][norm(x, size)];
    }

    return cube[in.face][norm(y, size)][norm((-x), size)];
}

double getPointValueFace(std::vector<std::vector<Direction>> topologie, int x, int y, int face, int size,
                         std::vector<std::vector<std::vector<double>>> &cube){

    if(x >= size) {
        return getValueNextFace(topologie[face][Cube::RIGHT_EDGE], Direction(face, Cube::RIGHT_EDGE), x, y, size, cube);
    }
    if(x < 0) {
        return getValueNextFace(topologie[face][Cube::LEFT_EDGE], Direction(face, Cube::LEFT_EDGE), x, y, size, cube);
    }
    if (y >= size) {
        return getValueNextFace(topologie[face][Cube::BOTTOM_EDGE], Direction(face, Cube::BOTTOM_EDGE), x, y, size, cube);
    }
    if (y < 0) {
        return getValueNextFace(topologie[face][Cube::TOP_EDGE], Direction(face, Cube::TOP_EDGE), x, y, size, cube);
    }

    return cube[face][x][y];
}

int normCoord(int x, int size){
    if(x >= size - 1){
        return x - size + 1;
    }
    if(x <= 0){
        x = x + size - 1;
    }
    if(x <= 0){
        x = x + size - 1;
    }

    return x;
}

glm::ivec2 getCoordNextFace(Direction in, Direction out, int x, int y, int size,
                            std::vector<std::vector<std::vector<double>>> &cube){
    std::vector<glm::vec3> movementVector{glm::vec3(-1,0,0), glm::vec3(0,1,0), glm::vec3(1,0,0), glm::vec3(0,-1,0)};

    auto crossProduct = glm::cross(movementVector[in.edge], movementVector[out.edge]).z;

    if(crossProduct == 0){

        if(in.edge == out.edge){
            return glm::ivec2(x, normCoord(size - 1 - y, size));
        }
        else{
            if(movementVector[in.edge].x == 0){
                return glm::ivec2(normCoord(x, size), normCoord(y,size));
            }
            else{
                return glm::ivec2(normCoord(x, size), normCoord(y, size));
            }
        }
    }
    if(crossProduct > 0){
        switch(in.edge){
            case Cube::BOTTOM_EDGE :
                return glm::ivec2(normCoord(y, size), x);
            case Cube::RIGHT_EDGE :
                return glm::ivec2(normCoord(y, size), normCoord(-x, size));
            case Cube::TOP_EDGE :
                return glm::ivec2(normCoord(y, size), x);
            case Cube::LEFT_EDGE :
                return glm::ivec2(normCoord(y, size), normCoord(-x, size));

        }
    }
    switch(in.edge){
        case Cube::BOTTOM_EDGE :
            return glm::ivec2(normCoord(-y, size), normCoord(x, size));
        case Cube::RIGHT_EDGE :
            return glm::ivec2(y, normCoord(x, size));
        case Cube::TOP_EDGE :
            return glm::ivec2(normCoord(-y, size), normCoord(x, size));
        case Cube::LEFT_EDGE :
            return glm::ivec2(y, normCoord(x, size));

    }
}

double moyenne_carre(int x, int y, int d, std::vector<std::vector<std::vector<double>>>& cube, int face, double coef, float gen){
    d >>= 1;

    double moyenne = (cube[face][x-d][y-d] + cube[face][x-d][y+d] + cube[face][x+d][y+d] + cube[face][x+d][y-d])/4;

    return moyenne+ (gen * coef);
}

double moyenne_diamant(int x, int y, int d, int size, std::vector<std::vector<std::vector<double>>>& cube, int face, int coef,
                       float gen){
    double somme = 0;
    int n = 4;

    d >>= 1;

    somme += getPointValueFace(topologie, x - d, y, face, size, cube);
    somme += getPointValueFace(topologie, x, y - d, face, size, cube);
    somme += getPointValueFace(topologie, x + d, y, face, size, cube);
    somme += getPointValueFace(topologie, x, y + d, face, size, cube);

    std::uniform_int_distribution<int> dis(-d, d);
    return ((somme / n) + (gen * coef));
}

glm::vec3 spherifyPoint(float radius, glm::vec3 &point, double amplitude, uint size, NormalArray &normalArray){
    auto length = sqrt((point.x * point.x) + (point.y * point.y) + (point.z * point.z));
    point.x = point.x/length;
    point.y = point.y/length;
    point.z = point.z/length;

    point *= (radius + amplitude/(size*8));

    return point;
}