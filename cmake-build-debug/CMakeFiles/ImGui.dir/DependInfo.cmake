# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "C:/Users/DoubleVV/CLionProjects/CleanPlanet/ImGui/imgui.cpp" "C:/Users/DoubleVV/CLionProjects/CleanPlanet/cmake-build-debug/CMakeFiles/ImGui.dir/ImGui/imgui.cpp.obj"
  "C:/Users/DoubleVV/CLionProjects/CleanPlanet/ImGui/imgui_demo.cpp" "C:/Users/DoubleVV/CLionProjects/CleanPlanet/cmake-build-debug/CMakeFiles/ImGui.dir/ImGui/imgui_demo.cpp.obj"
  "C:/Users/DoubleVV/CLionProjects/CleanPlanet/ImGui/imgui_draw.cpp" "C:/Users/DoubleVV/CLionProjects/CleanPlanet/cmake-build-debug/CMakeFiles/ImGui.dir/ImGui/imgui_draw.cpp.obj"
  "C:/Users/DoubleVV/CLionProjects/CleanPlanet/ImGui/imgui_impl_glfw.cpp" "C:/Users/DoubleVV/CLionProjects/CleanPlanet/cmake-build-debug/CMakeFiles/ImGui.dir/ImGui/imgui_impl_glfw.cpp.obj"
  "C:/Users/DoubleVV/CLionProjects/CleanPlanet/ImGui/imgui_impl_opengl3.cpp" "C:/Users/DoubleVV/CLionProjects/CleanPlanet/cmake-build-debug/CMakeFiles/ImGui.dir/ImGui/imgui_impl_opengl3.cpp.obj"
  "C:/Users/DoubleVV/CLionProjects/CleanPlanet/ImGui/imgui_widgets.cpp" "C:/Users/DoubleVV/CLionProjects/CleanPlanet/cmake-build-debug/CMakeFiles/ImGui.dir/ImGui/imgui_widgets.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "IMGUI_IMPL_API="
  "IMGUI_IMPL_OPENGL_LOADER_GLEW=1"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "C:/Libraries/Glew/Compiled/MinGW-8.1.0/Glew/include"
  "../ImGui"
  "../Common/Include"
  "C:/Libraries/glm/Compiled/MinGW-8.1.0/build/include"
  "C:/Libraries/glfw/Compiled/MinGW-8.1.0/glfw/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
