#version 330 core
struct Material {
     vec3 ambient;
     vec3 diffuse;
     vec3 specular;
     float shininess;
};

struct Light {
     vec3 position;
     vec3 direction;

     vec3 ambient;
     vec3 diffuse;
     vec3 specular;
};

in vec3 Normal;
in vec3 FragPos;
in vec3 vertex;

out vec4 FragColor;

uniform vec3 viewPos;
uniform Light light;
uniform Material material;

uniform float gradient;
uniform float radius;
uniform float colorDecal;

//
#define Hex2Vex3(hex) vec3((float((hex/256/256)%256)/256.f),(float((hex/256)%256)/256.f),(float((hex)%256)/256.f))


// https://coolors.co/app/5ed6ff-32c5ff-07caed-2f9ec6-0083b7
// https://coolors.co/690611-daad2a-b4753f-422b17-110b06
// bf8156-58991d-54b534-4c4c4c-ffffff
const vec3 colors[5] = vec3[](
  // Terrain
  Hex2Vex3(0xbf8156), Hex2Vex3(0x58991d), Hex2Vex3(0x54b534), Hex2Vex3(0x4c4c4c), Hex2Vex3(0xffffff)
);
const float nb_colors = 5.f;

vec4 doTerrainPlot( float normal_height )
{
    float color_index = ((colorDecal+normal_height - radius)/gradient) * nb_colors;
    if( color_index < 0.f ) color_index = 0.f;
    else if( (color_index) >= nb_colors-1.f ) color_index = nb_colors-1.f;

    float color_mix = (1.f-(ceil(color_index) - color_index));

    vec3 c1 = colors[int(floor(color_index))];
    vec3 c2 = colors[int(ceil(color_index))];
    return vec4(mix(c1,c2,color_mix), 1.0);
}

float length(vec3 vertex){
    return sqrt((vertex.x * vertex.x) + (vertex.y * vertex.y) + (vertex.z * vertex.z));
}

void main()
{
    vec4 color = doTerrainPlot(length(vertex));
    // Ambiant
    vec3 ambient = material.ambient * light.ambient;

    // Diffuse
    vec3 norm = normalize(Normal);
    vec3 lightDir = normalize(-light.direction);

    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = light.diffuse * (diff * material.diffuse);

    // Spec
    vec3 viewDir = normalize(viewPos - FragPos);
    vec3 reflectDir = reflect(-lightDir, norm);

    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    vec3 specular = light.specular * (spec * material.specular);

    vec3 result = ambient + diffuse + specular;
    FragColor = color * vec4(result, 1.0);
}